#!/bin/sh
cur_dir=`dirname $0`

if [ $# -eq 0 ]; then
  echo "usage: deploy.sh <environment> [service]"
  echo "  where environment is one of:"
  echo "  - development"
  echo "  - production"
  echo "  - local \n"
  return
fi

. "./$cur_dir/env/$1.sh"

config_path="$cur_dir/kustomize/overlays/$kubernetes_environment"

#switch kubernetes contexxt
kubectl config use-context "$kubernetes_context"

namespace="pijam"
if [ "$1" = "development" ]; then
  namespace="pijam-staging"
fi
 
if [ $# -eq 1 ]; then
  #create namespace
  kubectl create namespace $namespace --save-config
  
  #create docker registry secret
  kubectl create secret docker-registry canister -n $namespace \
      "--docker-server=$docker_registry" \
      "--docker-username=$docker_username" \
      "--docker-password=$docker_password" \
      "--docker-email=$docker_email" 
  
  #create clearbit api secret
  kubectl create secret generic clearbit -n $namespace \
      --from-literal "clearbit.api.key=$clearbit_api_key"
  
  #create mongo credentials secret
  kubectl create secret generic mongo -n $namespace \
      --from-literal "mongo.atlas.username=$mongo_username" \
      --from-literal "mongo.atlas.password=$mongo_password"   
      
  #create strip credentials secret
  kubectl create secret generic stripe -n $namespace \
      --from-literal "stripe.secret=$stripe_secret" 
else
  config_path="$config_path/$2"  
fi

#deploy app or service
echo "kustomize $config_path"
"$path_to_kustomize" build ./"$config_path" | kubectl apply -f -


